//
//  ViewController.m
//  OurOwnScrollview
//
//  Created by James Cash on 20-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "OurScrollView.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    OurScrollView *rootView = [[OurScrollView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:rootView];
    rootView.backgroundColor = [UIColor whiteColor];
    rootView.contentSize = CGSizeMake(300, 900);
//    rootView.clipsToBounds = YES;

    UIView *red    = [[UIView alloc] initWithFrame:CGRectMake(20,20 , 100,  100)];
    UIView *green  = [[UIView alloc] initWithFrame:CGRectMake(150,150, 150, 200)];
    UIView *blue   = [[UIView alloc] initWithFrame:CGRectMake(40,400, 200, 150)];
    UIView *yellow = [[UIView alloc] initWithFrame:CGRectMake(100,600, 180, 150)];

    red.backgroundColor = [UIColor redColor];
    green.backgroundColor = [UIColor greenColor];
    blue.backgroundColor = [UIColor blueColor];
    yellow.backgroundColor = [UIColor yellowColor];

    [rootView addSubview:red];
    [rootView addSubview:green];
    [rootView addSubview:blue];
    [rootView addSubview:yellow];
}

//
//- (void)viewDidAppear:(BOOL)animated
//{
//    // Could do it this way
////    CGRect newBounds = self.view.bounds;
////    newBounds.origin.y += 100;
////    self.view.bounds = newBounds;
//    // I prefer this way; more flexible & more "Declarative"
//    self.view.bounds = CGRectOffset(self.view.bounds, 0, 100);
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  OurScrollView.m
//  OurOwnScrollview
//
//  Created by James Cash on 20-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "OurScrollView.h"

@interface OurScrollView ()

@property (nonatomic,strong) UIPanGestureRecognizer *panRecoginzer;

@end

@implementation OurScrollView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _panRecoginzer = [[UIPanGestureRecognizer alloc]
                          initWithTarget:self action:@selector(handleScroll:)];
        [self addGestureRecognizer:_panRecoginzer];
    }
    return self;
}

- (void)handleScroll:(UIPanGestureRecognizer*)sender
{
    CGPoint mvmt = [sender translationInView:self];

//    NSLog(@"translation: %@", NSStringFromCGPoint(mvmt));

    // Negative because we're offsetting the coordinate system
    CGRect newBounds = CGRectOffset(self.bounds, -mvmt.x, -mvmt.y);
//    NSLog(@"New bounds = %@", NSStringFromCGRect(newBounds));
    
    newBounds.origin.y = MIN(newBounds.origin.y,
                             self.contentSize.height - newBounds.size.height);
    newBounds.origin.y = MAX(newBounds.origin.y, 0);

    newBounds.origin.x = MIN(newBounds.origin.x,
                             self.contentSize.width - newBounds.size.width);
    newBounds.origin.x = MAX(newBounds.origin.x, 0);

    self.bounds = newBounds;

    // We reset the translation here because otherwise the `mvmt` above would be cumulative since the gesture started, but we want to just move by how far the pan has gone since the last time this method was called
    // if we remove this line, we see that everything scrolls away very very fast
    [sender setTranslation:CGPointZero inView:self];
}

@end
